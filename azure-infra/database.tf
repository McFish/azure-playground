resource "azurerm_postgresql_flexible_server" "dbserver" {
  name                = "pgsql-bff-test"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  zone       = 2
  sku_name   = "B_Standard_B1ms"
  version    = 15
  storage_mb = 32768

  administrator_login    = var.db_username
  administrator_password = var.db_password

  tags = local.tags
}

resource "azurerm_postgresql_flexible_server_database" "dbserver" {
  name      = "backend"
  server_id = azurerm_postgresql_flexible_server.dbserver.id
  collation = "en_US.utf8"
  charset   = "utf8"
}