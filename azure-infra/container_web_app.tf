resource "azurerm_container_app_environment" "ca_environment" {
  name                           = "cae-bff-test"
  resource_group_name            = azurerm_resource_group.rg.name
  location                       = azurerm_resource_group.rg.location
  log_analytics_workspace_id     = azurerm_log_analytics_workspace.analytics.id
  tags                           = local.tags
}

resource "azurerm_container_app" "ca_app_backend" {
  name                         = "ca-bff-test-001"
  container_app_environment_id = azurerm_container_app_environment.ca_environment.id
  resource_group_name          = azurerm_resource_group.rg.name
  revision_mode                = "Single"
  tags                         = local.tags

  template {
    container {
      name   = "backend"
      image  = var.backend_image
      cpu    = 0.25
      memory = "0.5Gi"
    }
    min_replicas = 1
    max_replicas = 1
  }

  ingress {
    external_enabled = false
    allow_insecure_connections = true
    target_port      = 5000
    traffic_weight {
      latest_revision = true
      percentage      = 100
    }
  }

}

resource "azurerm_container_app" "ca_app_frontend" {
  name                         = "ca-bff-test-002"
  container_app_environment_id = azurerm_container_app_environment.ca_environment.id
  resource_group_name          = azurerm_resource_group.rg.name
  revision_mode                = "Single"
  tags                         = local.tags

  template {
    container {
      name   = "frontend"
      image  = var.frontend_image
      cpu    = 0.25
      memory = "0.5Gi"

      env {
        name = "HOST"
        value = "true"
      }
      env {
        name = "BACKEND_URI"
        value = "http://${azurerm_container_app.ca_app_backend.latest_revision_fqdn}"
      }
    }
    min_replicas = 1
    max_replicas = 1
  }

  ingress {
    external_enabled = true
    target_port      = 3000
    traffic_weight {
      latest_revision = true
      percentage      = 100
    }
  }
}

output "backend_instance_fqdn" {
  value = azurerm_container_app.ca_app_backend.latest_revision_fqdn
}

output "frontend_instance_fqdn" {
  value = azurerm_container_app.ca_app_frontend.latest_revision_fqdn
}

