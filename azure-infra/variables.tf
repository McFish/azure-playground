variable "location" {
  type        = string
  default     = "swedencentral"
  description = "Location for resources"
}

variable "tag_owner_email" {
  type        = string
  description = "Email of the resource owner for tag"
}

variable "rg_name" {
  type        = string
  description = "Name of the resource group"
}

variable "db_username" {
  type        = string
  sensitive   = true
  description = "Database username"
}

variable "db_password" {
  type        = string
  sensitive   = true
  description = "Database password"
}

variable "backend_image" {
  type        = string
  description = "Docker image for backend"
}

variable "frontend_image" {
  type        = string
  description = "Docker image for frontend"
}