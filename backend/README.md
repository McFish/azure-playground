## Testing API

Create message:

```
curl --json '{"content":"message"}' http://localhost:5000/messages
```

Read messages:

```
curl http://localhost:5000/messages
```
