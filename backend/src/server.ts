import Fastify from 'fastify';

interface MessageBody {
  content: string;
}

const messages: { id: string; content: string }[] = [];

const fastify = Fastify({
  logger: true
});

fastify.post<{ Body: MessageBody }>('/messages', {}, async (request, reply) => {
  const { content } = request.body;
  const msg = { id: crypto.randomUUID(), content };
  messages.push(msg);
  return msg;
});

fastify.get('/messages', async (request, reply) => {
  return messages;
});

(async () => {
  try {
    await fastify.listen({ host: '0.0.0.0', port: 5000 });
  } catch (err) {
    fastify.log.error(err);
  }
})();
