# To deploy app to azure:

Copy 'secret.tfvars.example' file to 'secret.tfvars' and fill in values. Then run one of following commands:

`terraform plan -var-file="secret.tfvars"`

`terraform apply -var-file="secret.tfvars"`
