import type { ActionFunctionArgs, MetaFunction } from "@remix-run/node";

import { Form, useLoaderData } from "@remix-run/react";

export const meta: MetaFunction = () => {
  return [
    { title: "Messagebox app" },
    { name: "description", content: "Welcome to messagebox!" },
  ];
};

export const loader = async () => {
  if (!process.env.BACKEND_URI) {
    return { messages: [] };
  }

  const rawRsp = await fetch(`${process.env.BACKEND_URI}/messages`);
  const rsp = (await rawRsp.json()) as { id: string; content: string }[];
  return { messages: rsp };
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const body = await request.formData();
  const content = body.get("content");

  await fetch(`${process.env.BACKEND_URI}/messages`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ content }),
  });

  return null;
};

export default function Index() {
  const { messages } = useLoaderData<typeof loader>();
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.8" }}>
      <h1>Messagebox:</h1>
      {messages.map((msg) => (
        <div key={msg.id}>
          {msg.content}
          <br />
        </div>
      ))}
      <hr />
      <Form method="POST" reloadDocument>
        <textarea name="content"></textarea>
        <br />
        <button type="submit">New message</button>
      </Form>
    </div>
  );
}
